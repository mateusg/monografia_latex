FILE = tcc

build:
	pdflatex $(FILE).tex
	makeglossaries $(FILE)
	bibtex $(FILE).aux
	pdflatex $(FILE).tex
	pdflatex $(FILE).tex

clean:
	rm *.aux *.bbl *.toc *.out *.nls *.nlo *.lof *.lot *.blg *.ilg *.glg *.glo *.gls *.glsdefs *.ist *.idx *.brf *.acn *.acr *.alg

all:
	clean()
	build()
	clean()
