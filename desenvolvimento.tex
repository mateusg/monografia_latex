\chapter{Extensão do Redmine para Gerenciamento com o Scrum}
\label{cap:extensaoScrumine}

O \redmine{}, apesar de ser um gerenciador de projetos e tarefas com muitas funcionalidades, não é orientado a nenhum processo de desenvolvimento de software específico. Para alguns processos, como o \scrum{}, ele pode ser considerado ``incompleto'', por não fornecer artefatos, características e comportamentos importantes para a gerência de projetos com essa metodologia.

A extensão desenvolvida neste trabalho especializa o \redmine{} para projetos de software que utilizam o processo \scrum, provendo uma visualização das atividades em forma de \emph{backlog}. Como mencionado na subseção \ref{subSec:productBacklog}, a visualização das atividades em um \productBacklog{} ordenado por prioridade proporciona à todos os envolvidos no projeto uma constante visão geral do mesmo: o que é de maior importância, o que está sendo trabalhado no momento e o que será trabalhado nas próximas \sprint{s}, tudo num único artefato. Isso evita, durante a \sprint{}, questões entre membros da equipe do tipo ``Terminei essa estória, o que farei agora?''. Eles sempre terão os \emph{backlogs} para consultar e identificar qual a próxima estória mais importante a ser desenvolvida.

\section{Módulo Scrum Management}
\label{sec:scrumManagement}

Na extensão, foi implementado um módulo de projeto do \redmine{} chamado de ``Scrum Management'' (ou Gerência Scrum). Esse módulo inclui 2 novos itens no menu do projeto: ``Backlogs'' e ``Scrum Settings'' (Configurações do \scrum), como pode ser visto na figura \ref{fig:scrumine5}. Ele tem como objetivo substituir o módulo de gestão de tarefas padrão do \redmine, o ``Issue Tracking'' (Rastreamento de problemas/tarefas), fornecendo a gerência de estórias através do \productBacklog{} e de cadastros simplificados de estórias e \sprint{s}. Entretanto, a extensão ainda permite que projetos não orientados ao \scrum{} possam sejam criados e gerenciados pelo módulo padrão ``Issue Tracking'' do \redmine{}.

\begin{figure}[ht]
  \caption{\label{fig:scrumine5} Menu de um projeto com o módulo Scrum Management ativado}

  \begin{center}
    \includegraphics[scale=0.4]{imagens/scrumine_project_menu.png}
  \end{center}

  \legend{Fonte: elaboração própria (2011)}
\end{figure}

\subsection{Seleção de módulos}
\label{subsec:selecaoModulos}

Na interface de cadastro de projetos, a seção de módulos foi modificada, para que dinamicamente restrinja a seleção de módulos de gerência de tarefas ou estórias a um só. Ou seja, só é possível selecionar \emph{Issue Tracking} ou \emph{Scrum Management}. Quando um é selecionado, o outro é automaticamente desativado. Além disso, uma dependência do módulo ``Time Tracking'' (controle de tempo gasto) aos módulos de gerência foi implementada, para que ele só possa ser selecionado quando um dos módulos de gerência estiver ativado, já que as horas só podem ser cadastradas se houver rastreamento de tarefas/estórias. O mesmo comportamento pode ser observado na página de configurações de um projeto, onde também é possível selecionar módulos. Essas restrições foram desenvolvidas através de uma biblioteca \emph{JavaScript} que já vem embutida no \redmine{}, o \emph{Prototype}.

Na figura \ref{fig:scrumine2}, um novo projeto está sendo criado e o módulo ``Issue Tracking'' está selecionado. Por isso, o ``Scrum Management'' está desativado. Uma dica foi acrescentada na parte inferior do formulário, explicando como os módulos funcionam:
\begin{figure}[ht]
  \caption{\label{fig:scrumine2} Seleção de módulos de um projeto - \emph{Issue Tracking} selecionado}

  \begin{center}
    \includegraphics[scale=0.35]{imagens/scrumine2.png}
  \end{center}

  \legend{Fonte: elaboração própria (2011)}
\end{figure}

Já na figura \ref{fig:scrumine3}, a situação está invertida. O módulo ``Scrum Management'' está selecionado e o ``Issue Tracking'' está desabilitado:
\begin{figure}[ht]
  \caption{\label{fig:scrumine3} Seleção de módulos de um projeto - \emph{Scrum Management} selecionado}

  \begin{center}
    \includegraphics[scale=0.35]{imagens/scrumine3.png}
  \end{center}

  \legend{Fonte: elaboração própria (2011)}
\end{figure}

Um outro caso é mostrado na figura \ref{fig:scrumine4}, onde nenhum dos dois módulos (``Issue Tracking'' ou ``Scrum Management'') estão selecionados, fazendo com que o módulo ``Time Tracking'' desapareça:
\begin{figure}[ht]
  \caption{\label{fig:scrumine4} Seleção de módulos de um projeto - sem módulo de gerência selecionado}

  \begin{center}
    \includegraphics[scale=0.4]{imagens/scrumine4.png}
  \end{center}

  \legend{Fonte: elaboração própria (2011)}
\end{figure}

\subsection{Backlogs}
\label{subsec:backlogs}

A página de \emph{backlogs} é a mais importante da extensão do \scrum. Nela, é possível visualizar as informações atuais do projeto, como mostra a figura \ref{fig:scrumine1}:

\begin{figure}[ht]
  \caption{\label{fig:scrumine1} Visualização dos backlogs da extensão do \redmine}

  \begin{center}
    \includegraphics[scale=0.30]{imagens/scrumine1.png}
  \end{center}

  \legend{Fonte: elaboração própria (2011)}
\end{figure}

À esquerda, encontra-se o \productBacklog, com todas as \sprint{s} não concluídas, suas estórias, e todas as estórias que ainda não foram incluídas em \sprint{s} na parte inferior. À direita, encontra-se o \emph{backlog} da \sprint{} atual e suas estórias. Os \sprintBacklog{s} na extensão funcionam como parte do \productBacklog. Tudo que for alterado em um, resulta na alteração imediata do outro, e vice-versa.

Nessa mesma página, é possível arrastar as estórias com um clique do mouse e ordená-las (priorizá-las) ou movê-las de uma \sprint{} para outra. Assim como a listagem tradicional de tarefas do \redmine, a página de \emph{backlogs} também permite que os usuários atualizem os estados das estórias, ao clicar no botão azul para ``Iniciar'' (em inglês, ``Start'') ou ``Finalizar'' (em inglês, ``Finish'') uma estória.

Alguns \emph{models} do \redmine{} que são semelhantes às entidades do \scrum{} foram reaproveitados no módulo \emph{Scrum Management}, como ``Issue'' (tarefa), que foi aproveitado como estória, e ``Version'' (versão), que foi aproveitado como \sprint{}. Nos casos onde foi necessário adicionar algum campo ou método que não existe no \emph{model} original, foram criadas \emph{migrations} (para criar os campos nas tabelas do banco de dados) e \emph{patches} (``remendos'' de código), para sobrescrever métodos das classes originais ou adicionar novos métodos. A figura \ref{fig:versionPatch} do apêndice \ref{appendix:patches} apresenta o \emph{patch} do \emph{model} ``Version''. Os \emph{patches} ficam armazenados no diretório ``lib'' da extensão, sendo carregados quando a aplicação é iniciada, através de uma chamada feita no arquivo ``init.rb'', como mostra a figura \ref{fig:initRB} do apêndice \ref{appendix:initRB}.

Nos backlogs, ícones foram adicionados para simbolizar os tipos de estórias. Lâmpadas representam funcionalidades, bóias representam atividades de suporte, insetos representam \emph{bugs} (problemas) e engrenagens representam outros tipos de estória, que podem ser criados pela interface administrativa do \redmine. As cores de fundo representam os estados das estórias. Se uma estória tiver sido cancelada, a cor de fundo fica vermelha. Se estiver concluída, fica verde, como ilustra a figura \ref{fig:scrumineDoneStory}.

\begin{figure}[ht]
  \caption{\label{fig:scrumineDoneStory} \productBacklog{} com estória finalizada}

  \begin{center}
    \includegraphics[scale=0.35]{imagens/scrumine_done_story.png}
  \end{center}

  \legend{Fonte: elaboração própria (2011)}
\end{figure}

% Mensagens de alerta são exibidas para que a equipe tenha controle dos \emph{backlogs}. Por exemplo, durante a seleção de estórias para uma \sprint{}, é verificado se a quantidade de trabalho adicionado na \sprint{} ultrapassa a velocidade da equipe para a mesma \sprint{}. Caso isso aconteça, um alerta será exibido. Da mesma forma,

\subsection{Divisão de estórias em tarefas}
\label{subsec:divisaoDeEstoriasEmTarefas}

Segundo \citeonline{knibergH07scrum}, estórias não deveriam ser muito pequenas, nem muito grandes (em termos de estimativa). Portanto, quando há uma estória muito grande, é recomendável dividí-la em estórias menores ou ``tarefas''. A diferença entre tarefas e estórias é simples: estórias são trabalhos que podem ser entregues e que o \productOwner{} tem que se preocupar. Tarefas são atividades que não podem ser entregues e que o \productOwner{} não precisa se preocupar. Esse tipo de divisão frequentemente revela trabalho adicional, que faz com que a estimativa de tempo aumente, mas resulta em um planejamento de \sprint{} mais realista.

Baseado nessa divisão, foi implementado na extensão o cadastro de tarefas por estória. Uma tarefa tem um nome, uma estimativa (não obrigatoriamente preenchida) e um campo que diz se ela foi concluída ou não. A figura \ref{fig:scrumineNewStory} apresenta o cadastro de tarefas no formulário de nova estória:

\begin{figure}[ht]
  \caption{\label{fig:scrumineNewStory} Formulário de cadastro de estória}

  \begin{center}
    \includegraphics[scale=0.35]{imagens/scrumine_story_tasks.png}
  \end{center}

  \legend{Fonte: elaboração própria (2011)}
\end{figure}

\subsection{Configurações do Scrum}
\label{subsec:configScrum}

Como foi mencionado na seção \ref{subSec:reuniaoPlanejamentoSprint}, velocidade por pontos de estória é uma das técnicas muito utilizadas com o \scrum{} atualmente para estimar estórias. A extensão provê o uso dessa técnica através da definição de escalas de estórias, que são conjuntos de valores padrões usados nas estimativas. Essas escalas são definidas, por projeto, na página de configurações do \scrum{}. Se a equipe preferir não usar escalas, ela pode selecionar o tipo ``Personalizada'', que permite que as estimativas sejam feitas manualmente.

As escalas de pontos de estória possíveis são:
\begin{alineas}
  \item Fibonacci: 0, 1, 2, 3, 5, 8, 13;
  \item Potências de 2: 0, 1, 2, 4, 8, 16;
  \item Linear: 0, 1, 2, 3, 4;
  \item Personalizada: estimativa manual.
\end{alineas}

Além de permitir configurar as escalas, a tela de configurações do \scrum{} permite também que outras informações a respeito da gestão de estórias sejam modificadas, como a velocidade padrão da equipe por \sprint{}, a ativação de tarefas por estória e a ativação do cálculo  de estimativa de estória automático, realizado a partir da soma das estimativas das tarefas pertencentes à estória. %A velocidade padrão da \sprint{} só afeta novas \sprint{s}.

A seguir, a figura \ref{fig:scrumineScrumSettings} apresenta a página de configurações \scrum{} de um projeto:

\begin{figure}[ht]
  \caption{\label{fig:scrumineScrumSettings} Página de configurações \scrum{} de um projeto}

  \begin{center}
    \includegraphics[scale=0.5]{imagens/scrumine_scrum_settings.png}
  \end{center}

  \legend{Fonte: elaboração própria (2011)}
\end{figure}

%melhoria
